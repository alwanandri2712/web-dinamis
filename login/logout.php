<?php
   session_start();
   session_destroy();
?>

<link rel="stylesheet" type="text/css" href="../assets/css/boostrap.min.css">
<div class="container">
<div class="row">
	<div class="col-md-12">
	<div class="card" >
		<div class="card-body">
			
			<div class="alert alert-success" align="center" role="alert">
				Anda berhasil Logout
			</div>		
	<br>
	<br>
		 Silahkan klik <a href="loginv2.php"><b>disini</b></a> untuk login kembali
		 <br>
		 <br>
		<p>Silahkan klik <a href="../index.php"><b>Disini</b></a> untuk kembali ke halaman awal</p>

</div>
</div>
</div>