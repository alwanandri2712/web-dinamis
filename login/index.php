<?php
session_start();
if(!isset($_SESSION['username'])) {
   header('location:login.php'); 
} else { 
   $username = $_SESSION['username']; 
}
?>

<link rel="stylesheet" type="text/css" href="../assets/css/boostrap.min.css">
<title>Halaman Sukses Login</title>
<div align='center'>
   Selamat Datang  <b><?php echo $username;?></b>
   <a class='btn btn-danger btn-sm' href="logout.php" ><b>Logout</b></a>
</div>